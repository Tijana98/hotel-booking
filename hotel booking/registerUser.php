<?php
include_once 'user/include/class.user.php'; 
$user=new User(); 
if(isset($_REQUEST[ 'submit'])) 
{ 
    extract($_REQUEST); 
    $register=$user->reg_user($fullname, $name, $password, $email); 
    if($register) 
    { 
        echo "
<script type='text/javascript'>
    alert('Your registration has been Successfully');
</script>"; 
        echo "
<script type='text/javascript'>
window.location.href = 'user/login.php';
</script>"; 
    } 
    else 
    {
        echo "
<script type='text/javascript'>
    alert('Registration failed! username or email already exists');
    
</script>";
    }
} 
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>User Panel</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="admin/css/reg.css" type="text/css">
    <script language="javascript" type="text/javascript">
        function submitreg() {
            var form = document.reg;
            if (form.fullname.value == "") {
                alert("Enter Name.");
                return false;
            } else if (form.name.value == "") {
                alert("Enter username.");
                return false;
            } else if (form.password.value == "") {
                alert("Enter Password.");
                return false;
            } else if (form.email.value == "") {
                alert("Enter email.");
                return false;
            }
        }
    </script>



<style>

    body{
        background-image: url('../images/bg3.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
    }
</style>
</head>

<body>
    <div class="container">
        <div class="well">
            <h2>Registration</h2>
            <hr>
            <form action="" method="post" name="reg">
                <div class="form-group">
                    <label for="fullname">Full Name:</label>
                    <input type="text" class="form-control" name="fullname" placeholder="example: Ivan Ilic" required>
                </div>
                <div class="form-group">
                    <label for="name">User Name:</label>
                    <input type="text" class="form-control" name="name" placeholder="exmple: ivaan" required>
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" name="email" placeholder="example: ivan@gmail.com" required>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="text" class="form-control" name="password" placeholder="abc123" required>
                </div>
                <button type="submit" class="btn btn-lg btn-primary button" name="submit" value="Register" onclick="return(submitreg());">Submit</button>

               <br>
                <div id="click_here">
                    <a href="user/login.php">Go to Login page</a>
                </div>


            </form>
        </div>
    </div>

</body>

</html>