var myIndex=0;
slide();
function slide()
{
    var i;
    var x=document.getElementsByClassName("mySlides");
    for(i=0; i<x.length; i++)
        {
            x[i].style.display="none";
        }
    myIndex++;
    if(myIndex>x.length)
        {
            myIndex=1;
        }
    x[myIndex-1].style.display="block";
    setTimeout(slide,1500);
}


//Statistics


$.fn.jQuerySimpleCounter = function( options ) {
    var settings = $.extend({
        start:  0,
        end:    100,
        easing: 'swing',
        duration: 400,
        complete: ''
    }, options );

    var thisElement = $(this);

    $({count: settings.start}).animate({count: settings.end}, {
        duration: settings.duration,
        easing: settings.easing,
        step: function() {
            var mathCount = Math.ceil(this.count);
            thisElement.text(mathCount);
        },
        complete: settings.complete
    });
};


//contact

document.querySelector('#contact-form').addEventListener('submit', (e) => {
    e.preventDefault();
    e.target.elements.name.value = '';
    e.target.elements.email.value = '';
    e.target.elements.message.value = '';
  });