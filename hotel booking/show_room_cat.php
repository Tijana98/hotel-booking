<?php
include_once 'admin/include/class.user.php'; 
$user=new User();


?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Hotel Booking</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    
    
    <style>
          
        .well {
            background: rgba(0, 0, 0, 0.7);
            border: none;
            height: 250px;
        }
        
        body {
            background-image: url('images/bg3.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        
        h4 {
            color: #ffbb2b;
        }
        h6
        {
            color: navajowhite;
            font-family:  monospace;
        }
        .nav{
            font-size: 16px;
        }


    </style>
    
    
</head>

<body>
    <div class="container">
      
      
    <img class="img-responsive" src="images/100.PNG" style="width:100%; height:180px;">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="aboutus.php">About us</a></li>
                        <li><a href="room.php">Accommodation</a></li>
                        
                        <li><a href="reservation.php">Online Reservation</a></li>
                        <li><a href="#section-team">Team</a></li>
                        <li><a href="user/login.php">Login</a></li>
                        <li><a href="registerUser.php">Register</a></li>
                        <li><a href="admin.php">Admin</a></li>
                    </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="admin.php?q=logout">
                            <button type="button" class="btn btn-danger">Logout</button>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        
        
        
        <?php
        $results_per_page = 5;
        
        $sql="SELECT * FROM room_category";
        $result = mysqli_query($user->db, $sql);
        $number_of_result = mysqli_num_rows($result); 
        
        $number_of_page = ceil ($number_of_result / $results_per_page);
        
        if (!isset ($_GET['page']) ) {  
            $page = 1;  
        } else {  
            $page = $_GET['page'];  
        }  
        
        
        $page_first_result = ($page-1) * $results_per_page;  
        
        
        $query = "SELECT *FROM room_category LIMIT " . $page_first_result . ',' . $results_per_page;  
        $result = mysqli_query($user->db, $query);
      
//               ********************************************** Show Room Category*********************** 
                while($row = mysqli_fetch_array($result))
                {
                    
                    echo "
                    <div class='row'>
                    <div class='col-md-3'></div>
                    <div class='col-md-6 well'>
                        <h4>".$row['hotel']."</h4>
                        <h6>".$row['address']."</h6>
                        <hr> 
                        <h6>".$row['roomname']."</h6>
                        <h6>No of Beds: ".$row['no_bed']." ".$row['bedtype']." bed.</h6>
                        <h6>Facilities: ".$row['facility']."</h6>
                        <h6>Price: ".$row['price']." RSD/night.</h6>
                    </div>
                    <div class='col-md-3'>
                        <a href='./booknow.php?roomname=".$row['roomname']."'><button class='btn btn-primary button'>Book Now</button> </a>
                    </div>   
                    </div>
                            
                        
                    
                         ";
                    
                    
                }
                
                
                          
       
        
        
        
        
        
        ?>
<nav align="center" aria-label="Page navigation example">
  <ul class="pagination">
        <?php
            for($page = 1; $page<= $number_of_page; $page++) {  
                echo '<li><a href = "room.php?page=' . $page . '">' . $page . ' </a></li>';  
            }
       ?>
  </ul>
</nav>

    </div>
    
    
    
    
    





    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>